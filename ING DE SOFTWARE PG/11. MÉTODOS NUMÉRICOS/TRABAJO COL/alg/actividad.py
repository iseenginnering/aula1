import csv
from tsnip import tcubicos

limit_i = 360
limit_f = 1082
i = 0
datos_arr = []

with open('datos.csv', newline='') as File:
    reader = csv.reader(File)

# creamos un arreglo con diccionarios con:
    # datatime
    # hora:min
    # correspondiente radicacion
    for row in reader:
        if i > limit_i and i < limit_f:
            rad_line = {
                i: row[0],
                'time': row[0][11]+row[0][12]+row[0][13]+row[0][14]+row[0][15],
                'rad': row[4]
            }
            datos_arr.append(rad_line)
            pass

        i = i+1

tcubicos(limit_i, limit_f, datos_arr)
